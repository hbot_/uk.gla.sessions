package uk.gla.sessions.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import uk.gla.sessions.MainActivity;
import uk.gla.sessions.content.CalendarProvider;
import uk.gla.sessions.data.CalendarEvent;
import uk.gla.sessions.data.Session;
import uk.gla.sessions.database.SessionsDatabaseHandler;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

public class TimerService extends Service {

	public final static String TAG = "TSERV";

	private ArrayList<Session> sessionArray = new ArrayList<Session>();
	private TimerBinder tBinder = new TimerBinder();
	
	private SessionsDatabaseHandler sdb;
	private CalendarProvider cProvider;

	@Override
	public IBinder onBind(Intent arg0) {
		// sessionArray.add(new Session(true, TAG));
		// SessionsDatabaseHandler sdb = new SessionsDatabaseHandler(this);
		// sdb.addSession(new Session("Mock Session MONTHLY", 5, "home",
		// Session.SESSION_MONTHLY));
		// sdb.addSession(new Session("Mock Session WEEKLY", 3, "home",
		// Session.SESSION_WEEKLY));
		// sdb.addSession(new Session("Mock Session DAILY", 1, "home",
		// Session.SESSION_DAILY));
		readAllSessions();
		createShakeSensor();
		initCalendarProvider();
		return tBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		mSensorManager.unregisterListener(mSensorListener);
		return super.onUnbind(intent);
	}

	public void addSession(Session session) {
		sessionArray.add(session);
	}

	public void removeSession(Session session) {
		sessionArray.remove(session);
		if (sessionArray.isEmpty())
			this.stopSelf();
	}

	public int getSessionCount() {
		return sessionArray.size();
	}

	public Session getSession(int arg) {
		return sessionArray.get(arg);
	}

	public void refresh() {
		sessionArray.clear();
		readAllSessions();
	}

	public void readAllSessions() {
		sdb = new SessionsDatabaseHandler(this);
		sessionArray.addAll(sdb.getAllSessions());
	}

	public class TimerBinder extends Binder implements Serializable {
		private static final long serialVersionUID = 4189229925850369970L;

		public TimerService getService() {
			return TimerService.this;
		}
	}

	/*** ACCELEROMETER STUFF ***/

	private SensorManager mSensorManager;
	private float mAccel; 			// acceleration apart from gravity
	private float mAccelCurrent; 	// current acceleration including gravity
	private float mAccelLast; 		// last acceleration including gravity
	
	private static final int EMPIRICALLY_DETERMINED_SHAKE_SENSOR_THRESHOLD_LOL = 14;

	private final SensorEventListener mSensorListener = new SensorEventListener() {

		@Override
		public void onSensorChanged(SensorEvent se) {
			float x = se.values[0];
			float y = se.values[1];
			float z = se.values[2];
			mAccelLast = mAccelCurrent;
			mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
			float delta = mAccelCurrent - mAccelLast;
			mAccel = mAccel * 0.9f + delta; // perform low-cut filter
			if (mAccel > EMPIRICALLY_DETERMINED_SHAKE_SENSOR_THRESHOLD_LOL){
				Log.d("SENSOR", "WAT");
				sendNotification();
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

	private void createShakeSensor() {
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensorManager.registerListener(mSensorListener,
				mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
		mAccel = 0.00f;
		mAccelCurrent = SensorManager.GRAVITY_EARTH;
		mAccelLast = SensorManager.GRAVITY_EARTH;
	}

	private void sendNotification() {
		
		NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(this)
		        .setSmallIcon(android.R.drawable.ic_menu_add)
		        .setContentTitle(getNotificationTitle())
		        .setContentText(getNotificationMessage());
		Intent resultIntent = new Intent(this, MainActivity.class);
		
		TaskStackBuilder stackBuilder = TaskStackBuilder.from(getApplication().getApplicationContext());
		stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
		    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		
		Notification n = mBuilder.getNotification();
		n.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
		
		mNotificationManager.notify(103, mBuilder.getNotification());		
	}
	
	private void initCalendarProvider(){
		cProvider = new CalendarProvider(this);
		if (cProvider.isEmpty())
			cProvider.runCalendarQueries();
	}
	
	private String getNotificationMessage(){
		CalendarEvent cEvent = cProvider.getLastEventOfTheDay();
		if (cEvent == null) return "Your calendar is free";
		
		long endTime = cEvent.getEndTime();
		long startTime = cEvent.getStartTime();
		
		Date d = new Date(endTime);
		Date e = new Date(startTime);
		
		String sentence = (Calendar.getInstance().getTimeInMillis() > endTime) ? " Calendar is free since " : " You have an event at ";
		String time = (Calendar.getInstance().getTimeInMillis() > endTime) ? 
				d.getHours() + ":" + String.format("%02d o'clock.", d.getMinutes()) :
				e.getHours() + ":" + String.format("%02d o'clock.", e.getMinutes());
		return sentence + time;
	}
	
	private String getNotificationTitle(){
		double d = Math.random();
		return "" + sessionArray.get((int) (d * 10) % (sessionArray.size() - 1)).getsTitle();
	}
}
