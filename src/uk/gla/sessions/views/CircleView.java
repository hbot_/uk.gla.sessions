package uk.gla.sessions.views;

import uk.gla.sessions.R;
import uk.gla.sessions.data.Session;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class CircleView extends View{

	private static final String TAG = "CVIEW";

	Paint paint;
	Paint secondaryPaint;
	Paint incrementPaint;
	Paint fillPaintDown;
	Paint fillPaintUp;
	Path path;

	float cx;
	float cy;
	float radius;

	float arcRadius;
	RectF arcOval = new RectF();
	float arcStartAngle = 270;
	float arcEndAngle = 1;
	float arcIncrement = 0.13333333f;
	
	int sessionFrequency;

	boolean touch = false;

	public CircleView(Context context) {
		super(context);
		init();
	}

	public CircleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CircleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {

		paint = new Paint();
		paint.setColor(Color.BLUE);
		paint.setStrokeWidth(10);
		paint.setStyle(Paint.Style.STROKE);
		

		RadialGradient gradientDown = new RadialGradient(360, 464.5f, 1000,
				Color.WHITE, Color.DKGRAY,
				android.graphics.Shader.TileMode.MIRROR);
		RadialGradient gradientUp = new RadialGradient(360, 464.5f, 750,
				Color.WHITE, Color.LTGRAY,
				android.graphics.Shader.TileMode.CLAMP);

		fillPaintDown = new Paint();
		fillPaintDown.setStrokeWidth(10);
		fillPaintDown.setStyle(Paint.Style.FILL);
		fillPaintDown.setShader(gradientDown);

		fillPaintUp = new Paint();
		fillPaintUp.setStrokeWidth(10);
		fillPaintUp.setStyle(Paint.Style.FILL);
		fillPaintUp.setShader(gradientUp);

		secondaryPaint = new Paint();
		secondaryPaint.setColor(Color.LTGRAY);
		secondaryPaint.setStrokeWidth(10);
		secondaryPaint.setStyle(Paint.Style.STROKE);
		secondaryPaint.setShadowLayer(20, 0, 0, Color.LTGRAY);

		incrementPaint = new Paint();
		incrementPaint.setStrokeWidth(10);
		incrementPaint.setStyle(Paint.Style.STROKE);

		setPaintColor(0, 0, sessionFrequency);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// oval containing the arc
		arcOval.set(cx - arcRadius, cy - arcRadius, cx + arcRadius, cy
				+ arcRadius);

		canvas.drawCircle(cx, cy, radius, paint); // outer circle
		if (touch)
			canvas.drawCircle(cx, cy, arcRadius - 2, fillPaintDown); // inner disk
		else																	
			canvas.drawCircle(cx, cy, arcRadius - 2, fillPaintUp); // inner disk

		canvas.drawCircle(cx, cy, arcRadius, secondaryPaint); // inner circle
		canvas.drawArc(arcOval, arcStartAngle, arcEndAngle, false, incrementPaint); // oval
	}
	
	@Override
	protected void onSizeChanged (int w, int h, int oldw, int oldh){
		super.onSizeChanged(w, h, oldw, oldh);
		
		cy = h / 2;
		cx = w / 2;
		radius = (float) ((w / 2) * 0.65);
		arcRadius = (float) (radius * 0.85);
		
		this.invalidate();
	}

	public void increment() {
		arcEndAngle += arcIncrement;
		this.invalidate();
	}
	
	public void setTimePassed(float value){
		arcEndAngle += value;
		this.invalidate();
	}

	public void setPaintColor(int outercolor, int innercolor, int sessionFrequency) {
		
		switch(sessionFrequency){
		case Session.SESSION_DAILY:
			paint.setColor(getResources().getColor(R.color.SessionOne));
			paint.setShadowLayer(4, 2, 2, getResources().getColor(R.color.SessionOne));
			incrementPaint.setColor(getResources().getColor(R.color.SessionIncrement));
			incrementPaint.setShadowLayer(4, 2, 2,getResources().getColor(R.color.SessionIncrement));
			break;
		case Session.SESSION_WEEKLY:
			paint.setColor(getResources().getColor(R.color.LightBlue));
			paint.setShadowLayer(4, 2, 2, getResources().getColor(R.color.LightBlue));
			incrementPaint.setColor(getResources().getColor(R.color.LightPink));
			incrementPaint.setShadowLayer(4, 2, 2,getResources().getColor(R.color.LightPink));
			break;
		case Session.SESSION_MONTHLY:
			paint.setColor(getResources().getColor(R.color.BurlyWood));
			paint.setShadowLayer(4, 2, 2, getResources().getColor(R.color.BurlyWood));
			incrementPaint.setColor(getResources().getColor(R.color.LightSteelBlue));
			incrementPaint.setShadowLayer(4, 2, 2,getResources().getColor(R.color.LightSteelBlue));
			break;
		default:
			paint.setColor(getResources().getColor(R.color.Black));
			break;
		}
		secondaryPaint.setColor(getResources().getColor(R.color.LightGrey));
		secondaryPaint.setShadowLayer(4, 2, 2,
				getResources().getColor(R.color.LightGrey));
	}

	public void setSessionFrequency(int sFreq){
		sessionFrequency = sFreq;
		setPaintColor(0, 0, sessionFrequency);
	}
	
	public void setArcIncrement(float f) {
		arcIncrement = f;
	}

	private float distanceFromCenter(float x, float y) {
		double dx = cx - x;
		double dy = cy - y;
		double d = Math.sqrt((Math.pow(dx, 2) + Math.pow(dy, 2)));
		return (float) d;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);

		float x = event.getX();
		float y = event.getY();
		
		if (distanceFromCenter(x, y) <= radius) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				touch = true;					
				break;
			case MotionEvent.ACTION_MOVE:
				touch = true;
				break;
			case MotionEvent.ACTION_UP:
				touch = false;
				break;
			default:
				touch = false;
				break;
			}
			this.invalidate();
			return true;
		}
		return false;
	}

	@Override
	protected void onFocusChanged(boolean gainFocus, int direction,
			Rect previouslyFocusedRect) {
		super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
		this.invalidate();
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		super.onWindowFocusChanged(hasWindowFocus);
		this.invalidate();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//		if (this.getHeight() > 0) {
//			
//			cy = this.getHeight() / 2;
//			cx = this.getWidth() / 2;
//			radius = (float) ((this.getWidth() / 2) * 0.65);
//			arcRadius = (float) (radius * 0.85);
		//}
	}
}
