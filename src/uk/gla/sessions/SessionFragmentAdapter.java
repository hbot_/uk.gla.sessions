package uk.gla.sessions;

import uk.gla.sessions.services.TimerService;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class SessionFragmentAdapter extends FragmentPagerAdapter {
	
	private static final String TAG = "SFADAPTER";
	
	TimerService tService;
	
	public SessionFragmentAdapter(FragmentManager fm, TimerService service) {
		super(fm);
		tService = service;
	}

	@Override
	public Fragment getItem(int arg0) {
		return MainFragment.newInstance(tService.getSession(arg0));
	}

	@Override
	public int getCount() {
		return tService.getSessionCount();
	}
}
