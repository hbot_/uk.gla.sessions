package uk.gla.sessions.database;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import uk.gla.sessions.data.Session;
import uk.gla.sessions.data.SessionInstance;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SessionsDatabaseHandler extends SQLiteOpenHelper {
	
	public static final int DATABASE_VERSION = 3;
	
	// 
    private static final String DATABASE_NAME = "SessionsLocalDatabase";
    // 
    private static final String TABLE_SESSIONS = "Sessions";
    private static final String TABLE_INSTANCES = "Instances";
 
    // Sessions table columns
    private static final String SESSION_ID = "session_id";
    private static final String SESSION_TITLE = "session_title";
    private static final String SESSION_DURATION = "session_duration";
    private static final String SESSION_LOCATION = "session_location";
    private static final String SESSION_FREQUENCY = "session_frequency";
    
    // Instances table columns
    private static final String INSTANCE_ID = "instance_id";
    private static final String INSTANCE_SESSION_ID = "instance_session_id";
    private static final String INSTANCE_START_TIME = "start_time";
    private static final String INSTANCE_END_TIME = "end_time";
    private static final String INSTANCE_LOCATION = "location";
    

    public SessionsDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_sessions_table = "CREATE TABLE " + TABLE_SESSIONS + "(" + 
        		SESSION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
        		SESSION_TITLE + " TEXT NOT NULL," + 
        		SESSION_DURATION + " INTEGER, " + 
        		SESSION_LOCATION + " TEXT, " + 
        		SESSION_FREQUENCY + " INTEGER " + ")";
        
        String create_instances_table = "CREATE TABLE " + TABLE_INSTANCES + "(" +
        		INSTANCE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + 
        		INSTANCE_START_TIME + " INT, " +
        		INSTANCE_END_TIME + " INT, " +
        		INSTANCE_LOCATION + " TEXT, " +
        		INSTANCE_SESSION_ID + " INTEGER," +
        		" FOREIGN KEY (" + INSTANCE_SESSION_ID + ") REFERENCES " + TABLE_SESSIONS + " (" + SESSION_ID +"));";
        
        db.execSQL(create_sessions_table);
        db.execSQL(create_instances_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INSTANCES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SESSIONS);
        onCreate(db);
    }
    
    // 
    public void addSession(Session s) {    	
    	SQLiteDatabase db = this.getWritableDatabase();	 
        ContentValues values = new ContentValues();
        
        values.put(SESSION_TITLE, s.getsTitle());
        values.put(SESSION_DURATION, s.getsDuration());
        values.put(SESSION_LOCATION, s.getsLocation());
        values.put(SESSION_FREQUENCY, s.getSessionFrequency());
     
        db.insertWithOnConflict(TABLE_SESSIONS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        db.close(); 
    }
    
    private String[] sessionCols = { SESSION_TITLE, SESSION_DURATION, SESSION_LOCATION, SESSION_FREQUENCY, SESSION_ID };
    
    private static final int S_TITLE = 0; //String
    private static final int S_DURATION = 1; //Integer
    private static final int S_LOCATION = 2; //String
    private static final int S_FREQUENCY = 3; //Integer
    private static final int S_ID = 4;
     
    // 
    public Session getSession(int sessionID) {
    	SQLiteDatabase db = this.getReadableDatabase();

    	Cursor cursor = db.query(TABLE_SESSIONS, sessionCols, 
        		SESSION_ID + "=?", new String[] { sessionID + "" }, null, null, null, null);
        
    	if (cursor == null)
    		return null;
    	
    	cursor.moveToFirst();
    	Session session = new Session( cursor.getString(S_TITLE), 
        		cursor.getInt(S_DURATION),
        		cursor.getString(S_LOCATION),
        		cursor.getInt(S_FREQUENCY),
        		cursor.getInt(S_ID));
        cursor.close();
        db.close();
        return session;   
    }
    
    public Session getSession(String name) {
    	SQLiteDatabase db = this.getReadableDatabase();

    	Cursor cursor = db.query(TABLE_SESSIONS, sessionCols, 
        		SESSION_TITLE + "=?", new String[] { name }, null, null, null, null);
        
    	if (cursor == null)
    		return null;
    	
    	cursor.moveToFirst();
    	Session session = new Session( cursor.getString(S_TITLE), 
        		cursor.getInt(S_DURATION),
        		cursor.getString(S_LOCATION),
        		cursor.getInt(S_FREQUENCY),
        		cursor.getInt(S_ID));
        cursor.close();
        db.close();
        return session;   
    }
    
    public int getSessionID(String name){
    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor cursor = db.query(TABLE_SESSIONS, sessionCols, 
        		SESSION_TITLE + "=?", new String[] { name }, null, null, null, null);
    	if (cursor == null)
    		return -1;
    	cursor.moveToFirst();
    	db.close();
    	return cursor.getInt(S_ID);
    	
    }
    
     
    // 
    public List<Session> getAllSessions() {
    	List<Session> sessionList = new ArrayList<Session>();
        String selectQuery = "SELECT  * FROM " + TABLE_SESSIONS + " ORDER BY " + SESSION_FREQUENCY + " ASC ";
     
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        
        int sTitleIndex = cursor.getColumnIndex("session_title");
        int sDurationIndex = cursor.getColumnIndex("session_duration");
        int sDurationLocation = cursor.getColumnIndex("session_location");
        int sDurationFrequency = cursor.getColumnIndex("session_frequency");
     
        if (cursor.moveToFirst()) {
            do {
                Session session = new Session(
                		cursor.getString(sTitleIndex),
                		cursor.getInt(sDurationIndex),
                		cursor.getString(sDurationLocation),
                		cursor.getInt(sDurationFrequency)
                		);
                sessionList.add(session);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return sessionList;
    }
         
    public int updateSession(Session session) {
    	SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SESSION_TITLE, session.getsTitle());
        return db.update(TABLE_SESSIONS, values, SESSION_TITLE + " = ?",
                new String[] { session.getsTitle() });
    }
     
    public void deleteSession(Session session) {
    	SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SESSIONS, SESSION_TITLE + " = ?",
                new String[] { session.getsTitle() });
        db.close();
    }
    
/*** INSTANCE TABLE ACCESS ***/
    
    public void addInstance(SessionInstance instance){
    	this.addInstance(instance.getEndTime().getTime(), instance.getStartTime().getTime(),
    			instance.getLocation(), instance.getSessionID());
    }
    
    public void addInstance(long endTime, long startTime, String location, int sessionID){
     	SQLiteDatabase db = this.getWritableDatabase();	 
        ContentValues values = new ContentValues();
        
        values.put(INSTANCE_START_TIME, startTime);
        values.put(INSTANCE_END_TIME, endTime);
        values.put(INSTANCE_LOCATION, location);
        values.put(INSTANCE_SESSION_ID, sessionID);
     
        db.insertWithOnConflict(TABLE_INSTANCES, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        db.close(); 
    }
    
    
    public List<SessionInstance> getAllInstance(int period) {
    	SQLiteDatabase db = this.getReadableDatabase();
    	List<SessionInstance> instanceList = new ArrayList<SessionInstance>();
        String selectQuery = "SELECT  * FROM " + TABLE_SESSIONS + 
        					 " WHERE ( " + INSTANCE_END_TIME + " < ? " +
        					 " AND " + INSTANCE_START_TIME + " > ? )";
        
        String [] cols = {INSTANCE_END_TIME, INSTANCE_START_TIME, INSTANCE_LOCATION, INSTANCE_ID, INSTANCE_SESSION_ID};
        
        Cursor cursor = db.query(TABLE_INSTANCES, cols, 
        		selectQuery, new String[] { today() + "", getEndPeriod(period) + "" }, null, null, null, null);
        
        int instanceID = cursor.getColumnIndex(INSTANCE_ID);
        int instanceSessionID = cursor.getColumnIndex(INSTANCE_SESSION_ID);
        int instanceLocation = cursor.getColumnIndex(INSTANCE_LOCATION);
        int instanceStartTime = cursor.getColumnIndex(INSTANCE_START_TIME);
        int instanceEndTime = cursor.getColumnIndex(INSTANCE_END_TIME);
     
        if (cursor.moveToFirst()) {
            do {
                SessionInstance instance = new SessionInstance(
                		new Date(cursor.getInt(instanceStartTime)),
                		new Date(cursor.getInt(instanceEndTime)),
                		cursor.getString(instanceLocation),
                		cursor.getInt(instanceSessionID),
                		cursor.getInt(instanceID)
                		);
                instanceList.add(instance);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return instanceList;
    }
    
    private static long getEndPeriod(int period){
    	switch(period){
    	case SessionInstance.PAST_DAY: return yesterday();
    	case SessionInstance.PAST_WEEK: return lastWeek();
    	case SessionInstance.PAST_MONTH: return lastMonth();
    	default: return lastWeek();
    	}
    }

	private static long today(){
		return Calendar.getInstance().getTimeInMillis();
	}

	private static long yesterday(){
		return today() - 24 * 60 * 60 * 1000;
	}
	
	private static long lastWeek(){
		return today() - 7 * 24 * 60 * 60 * 1000;
	}
	
	private static long lastMonth(){
		return today() - 30 * 7 * 24 * 60 * 60 * 1000;
	}
}
