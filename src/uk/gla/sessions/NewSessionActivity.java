package uk.gla.sessions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class NewSessionActivity extends Activity {
	
	EditText sName;
	EditText sDuration;
	EditText sLocation;
	Spinner sFreq;
	
	@Override 
	public void onCreate(Bundle s){
		super.onCreate(s);
		setContentView(R.layout.activity_new);
		
		sName = (EditText) findViewById(R.id.editSessionName);
		sDuration = (EditText) findViewById(R.id.editSessionDuration);
		sLocation = (EditText) findViewById(R.id.editSessionLocation);
		sFreq = (Spinner) findViewById(R.id.spinnerSessionFrequency);
		
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.session_frequency_array, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		sFreq.setAdapter(adapter);
		
		
		findViewById(R.id.dialogButtonCancel).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent returnIntent = new Intent();
				setResult(RESULT_CANCELED, returnIntent);        
				finish();
			}
		});
		
		findViewById(R.id.dialogButtonAdd).setOnClickListener(new OnClickListener() {					
			@Override
			public void onClick(View v) {
				Intent returnIntent = new Intent();
				returnIntent.putExtra("newSession", NewSessionActivity.this.getTextArray());
				
				setResult(RESULT_OK, returnIntent);
				finish();
			}
		});
	}
	
	private String[] getTextArray(){
		String[] array = new String[4];
		array[0] = sName.getText().toString();
		array[1] = sDuration.getText().toString();
		array[2] = sLocation.getText().toString();
		array[3] = sFreq.getSelectedItem().toString();
		return array;
	}
}
