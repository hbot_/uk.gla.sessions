package uk.gla.sessions.content;

import java.util.ArrayList;

import uk.gla.sessions.data.CalendarEvent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Instances;

public class CalendarProvider {

	private Context context;
	private ArrayList<CalendarEvent> tomorrowsEvents = new ArrayList<CalendarEvent>();
	private ArrayList<CalendarEvent> tomorrowsDeadlines = new ArrayList<CalendarEvent>();
	
	private static final String USERNAME = "horatiubota@gmail.com";
	private static final String CALENDAR_TYPE = "com.google";
	private static final String DEADLINES = "Deadlines";
	
	public static final String[] CALENDAR_PROJECTION = new String[] { Calendars._ID };

	public static final String[] EVENT_PROJECTION = new String[] { Events._ID };

	public static final String[] INSTANCES_PROJECTION = new String[] {
			Instances.EVENT_ID,  // 0
			Instances.BEGIN,	 // 1
			Instances.END,		 // 2
			Instances.TITLE 	 // 3
	};

	// The indices for the projection array above.
	private static final int CALENDAR_PROJECTION_ID_INDEX = 0;

	private static final int EVENT_PROJECTION_ID_INDEX = 0;

	// The indices for the projection array above.
	private static final int INSTANCES_PROJECTION_ID_INDEX = 0;
	private static final int INSTANCES_PROJECTION_BEGIN_INDEX = 1;
	private static final int INSTANCES_PROJECTION_END_INDEX = 2;
	private static final int INSTANCES_PROJECTION_TITLE_INDEX = 3;

	public CalendarProvider(Context ctx) {
		super();
		context = ctx;
	}
	
	public void runCalendarQueries(){
		tomorrowsEvents = getEvents(getCalendars(USERNAME, CALENDAR_TYPE, USERNAME));
		tomorrowsDeadlines = getEvents(getCalendars(USERNAME, CALENDAR_TYPE, DEADLINES));
	}
	
	public boolean isThereADeadlineTomorrow(){
		return (tomorrowsDeadlines == null) ? false : !tomorrowsDeadlines.isEmpty();
	}
	
	public boolean isEmpty(){
		return (tomorrowsDeadlines.isEmpty() && tomorrowsEvents.isEmpty());
	}
	
	public ArrayList<CalendarEvent> getTomorrowsEvents(){
		return tomorrowsEvents;
	}
	
	public CalendarEvent getLastEventOfTheDay(){
		return (tomorrowsEvents.isEmpty()) ? null : tomorrowsEvents.get(tomorrowsEvents.size() - 1);
	}

	public long getCalendars(String userName, String calendarType, String calendarName) {
		// Run query
		Cursor cur = null;
		ContentResolver cr = context.getContentResolver();
		Uri uri = Calendars.CONTENT_URI;
		String selection = "((" + Calendars.ACCOUNT_NAME + " = ?) AND ("
				+ Calendars.ACCOUNT_TYPE + " = ?) AND ("
				+ Calendars.CALENDAR_DISPLAY_NAME + " = ?))";
		String[] selectionArgs = new String[] { userName, calendarType, calendarName };
		cur = cr.query(uri, CALENDAR_PROJECTION, selection, selectionArgs, null);

		long calID = 0;
		while (cur.moveToNext()) {
			calID = cur.getLong(CALENDAR_PROJECTION_ID_INDEX);
		}
		return calID;
	}

	public ArrayList<CalendarEvent> getEvents(long calID) {
		Cursor cur = null;
		ContentResolver cr = context.getContentResolver();
		Uri uri = Events.CONTENT_URI;
		String selection = "((" + Events.CALENDAR_ID + " = ?))";
		String[] selectionArgs = new String[] { calID + "" };
		cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);
		
		ArrayList<CalendarEvent> rArray = new ArrayList<CalendarEvent>();
		while (cur.moveToNext()) {
			long eventID = 0;
			eventID = cur.getLong(EVENT_PROJECTION_ID_INDEX);
			rArray.addAll(getTomorrowsInstances(eventID));
		}
		return rArray;
	}

	public ArrayList<CalendarEvent> getTomorrowsInstances(long eventID) {
		long today = System.currentTimeMillis();
		long tomorrow = today + 86400000;
		ArrayList<CalendarEvent> rArray = new ArrayList<CalendarEvent>();
		
		Cursor cur = null;
		ContentResolver cr = context.getContentResolver();
		
		String selection = Instances.EVENT_ID + " = ?";
		String[] selectionArgs = new String[] { eventID + "" };

		Uri.Builder builder = Instances.CONTENT_URI.buildUpon();
		ContentUris.appendId(builder, today);
		ContentUris.appendId(builder, tomorrow);

		cur = cr.query(builder.build(), INSTANCES_PROJECTION, selection,
				selectionArgs, null);
				
		String title = null;
		long instanceID = 0;
		long endVal = 0;
		long beginVal = 0;
		while (cur.moveToNext()) {
			instanceID = cur.getLong(INSTANCES_PROJECTION_ID_INDEX);
			endVal = cur.getLong(INSTANCES_PROJECTION_END_INDEX);
			beginVal = cur.getLong(INSTANCES_PROJECTION_BEGIN_INDEX);
			title = cur.getString(INSTANCES_PROJECTION_TITLE_INDEX);
			rArray.add(new CalendarEvent(instanceID, beginVal, endVal, title));
		}
		return rArray;
	}
}
