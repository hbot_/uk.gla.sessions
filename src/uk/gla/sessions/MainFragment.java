package uk.gla.sessions;

import java.util.Calendar;

import uk.gla.sessions.data.CountDownTimerWithPause;
import uk.gla.sessions.data.Session;
import uk.gla.sessions.data.SessionInstance;
import uk.gla.sessions.database.SessionsDatabaseHandler;
import uk.gla.sessions.views.CircleView;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainFragment extends Fragment{
	
	private static final String TAG = "MFRAG";
	
	public CircleView cView;
	public TextView sTitle;
	public TextView sDurationTime;
	public TextView sDurationUnit;
	public TextView sTimerStatus;	

	private Session session;
	private LocalTimer localTimer;
	private Typeface sFont;
	
	public static MainFragment newInstance(Session s) {
		MainFragment fragment = new MainFragment();
		
		Bundle bundle = new Bundle();
		bundle.putSerializable("session", s);
		fragment.setArguments(bundle);
		
		return fragment;
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = getArguments() != null ? (Session)getArguments().getSerializable("session") : new Session(true, TAG);
        
        localTimer = new LocalTimer(session);
        localTimer.create();
        session.getTimer().create();
        
        sFont = Typeface.createFromAsset(getActivity().getAssets(), "Dosis-Light.ttf");
    }
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
    	RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.fragment_main, container, false);
    	
    	cView = (CircleView) layout.findViewById(R.id.sessionCircles);
    	sTitle = (TextView) layout.findViewById(R.id.sessionTitle);
    	sDurationTime = (TextView) layout.findViewById(R.id.sessionDurationLabel);
    	sDurationUnit = (TextView) layout.findViewById(R.id.sessionDurationUnitLabel);
    	sTimerStatus = (TextView) layout.findViewById(R.id.sessionTimerStatus);
    	setFont(sFont);
    	
		cView.setArcIncrement(localTimer.getArcIncrement());
		cView.setTimePassed(localTimer.getArcElapsed());
		cView.setSessionFrequency(session.getSessionFrequency());
		Log.d(TAG, "Freq: " + session.getSessionFrequency() + "");
        
		sTitle.setText(session.getsTitle());
        sDurationTime.setText(localTimer.getMinutesLeft() + "");
        setMessages();
        
        cView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (localTimer.isRunning()){
					pauseTimers();
				}
				else if (localTimer.isPaused()) {
					resumeTimers();
				}
			}
		});
        
    	return layout;
    }
    
    @Override 
	public void onResume(){
		super.onResume();
		if (localTimer == null){
			localTimer = new LocalTimer(session);
		}
	}
	
	@Override
	public void onPause(){
		super.onPause();
	}
	
	@Override
	public void onDestroyView(){
		super.onDestroyView();
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
	}
	
	private void pauseTimers(){
		localTimer.pause();
		session.getTimer().pause();
	}

	private void resumeTimers(){
		localTimer.resume();
		session.getTimer().resume();
	}
	
    private void setFont(Typeface tf){
    	sTitle.setTypeface(tf);
    	sDurationTime.setTypeface(tf);
    	sDurationUnit.setTypeface(tf);
    	sTimerStatus.setTypeface(tf);
    }
    
    private void setMessages(){
    	String min = (localTimer.timeLeft() < 2000 && localTimer.timeLeft() > 1000) ? "second" : "seconds";
    	String status = "Default";
    	if (localTimer.timePassed() == 0)
    		status = "Start";
    	else if (localTimer.isPaused())
    		status = "Paused";
    	else if (localTimer.isRunning())
    		status = "Running";
    	else if (!localTimer.isRunning()) 
    		status = "Finished";
    	sDurationTime.setText(localTimer.getSecondsLeft() + "");
    	sDurationUnit.setText(min);
    	sTimerStatus.setText(status);
    }
    
    private class LocalTimer extends CountDownTimerWithPause {
    	
    	long millisOnTimer;
    	Session s;

		public LocalTimer(long millisOnTimer, long countDownInterval,
				boolean runAtStart) {
			super(millisOnTimer, countDownInterval, runAtStart);
		}
		
		public LocalTimer (Session s){
			super (s.getTimer().getMillisOnTimer(), s.getTimer().getCountDownInterval(), s.getTimer().getRunAtStart());
			this.s = s;
			
			millisOnTimer = s.getTimer().getMillisOnTimer();
		}
		
		public float getArcIncrement(){
			return  360 / (s.getsDurationInMillis() / 1000);
		}
		
		public float getArcElapsed(){
			return (s.getsDurationInMillis() - millisOnTimer) / 1000 * getArcIncrement();
		}
		
		public int getMinutesLeft(){
			return (int) millisOnTimer / (60 * 1000);
		}
		
		public int getSecondsLeft(){
			return (int) millisOnTimer / 1000;
		}

		@Override
		public void onTick(long millisUntilFinished) {
			millisOnTimer = millisUntilFinished;			
			cView.increment();
			setMessages();
		}

		@Override
		public void onFinish() {
			millisOnTimer = 0;
			cView.increment();
			setMessages();
			writeInstanceToDB();
		}
		
		@Override
		public void onPause(){
			setMessages();
		}
		
		@Override
		public void onResume(){
			setMessages();
		}
    }
    
    private void writeInstanceToDB(){
		SessionsDatabaseHandler sdb = new SessionsDatabaseHandler(getActivity());
		int sessionID = sdb.getSessionID(session.getsTitle());
		Calendar now = Calendar.getInstance();
		long startTime = now.getTimeInMillis() - session.getsDurationInMillis();
		sdb.addInstance(startTime, now.getTimeInMillis(), session.getsLocation(), sessionID);
    }
}
