package uk.gla.sessions.data;

import java.util.Date;

public class SessionInstance {
	
	public static final int PAST_DAY = 19;
	public static final int PAST_WEEK = 21;
	public static final int PAST_MONTH = 23;
	
	private Date startTime;
	private Date endTime;
	private String location;
	private int sessionID;
	
	public SessionInstance(Date startTime, Date endTime, String location,
			int sessionID, int instanceID) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.location = location;
		this.sessionID = sessionID;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getSessionID() {
		return sessionID;
	}
	public void setSessionID(int sessionID) {
		this.sessionID = sessionID;
	}
}
