package uk.gla.sessions.data;

public class CalendarEvent {
	
	long eventID;
	long startTime;
	long endTime;
	String title;
	boolean isLast;

	public CalendarEvent() {
		eventID = 0;
		startTime = 0;
		endTime = 0;
		title = null;
		isLast = true;
	}
	
	public CalendarEvent(long eventID, long startTime, long endTime, String title) {
		super();
		this.eventID = eventID;
		this.startTime = startTime;
		this.endTime = endTime;
		this.title = title;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
