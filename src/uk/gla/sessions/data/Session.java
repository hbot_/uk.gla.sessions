package uk.gla.sessions.data;

import java.io.Serializable;


public class Session implements Serializable {
	
	private static final long serialVersionUID = -358683084971628125L;

	//private static final String TAG = "SESSION";
	
	public static final int SESSION_DAILY = 0;
    public static final int SESSION_WEEKLY = 1;
    public static final int SESSION_MONTHLY = 2;
	
	SessionTimer sTimer;
	String sTitle;
	String sDuration;
	String sLocation;
	String sFreq;
	int sessionID;
	
	public Session(){
	}
	
	public Session (String title, int duration, String location, int frequency){
		sTitle = title;
		sDuration = duration + "";
		sLocation = location;
		sFreq = sessionFrequencyToString(frequency);
		sTimer = new SessionTimer(getsDurationInMillis(), getsDurationInMillis() / 60, false);
		sessionID = -1;
	}
	
	public Session (String title, int duration, String location, int frequency, int sessionID){
		this(title,duration,location,frequency);
		this.sessionID = sessionID;
	}

	public Session(String[] a){
		sTitle = a[0];
		sDuration = a[1];
		sLocation = a[2];
		sFreq = a[3];
		sTimer = new SessionTimer(getsDurationInMillis(), getsDurationInMillis() / 60, false);
	}
	
	public Session(boolean isMock, String tag){
		sTitle = "Mock Session " + tag;
		sDuration = "1";
		sLocation = "Home";
		sFreq = "weekly";
		sTimer = new SessionTimer(5 * 1000, 1 * 1000, false);
	}
	
	public String getsTitle() {
		return sTitle;
	}

	public void setsTitle(String sTitle) {
		this.sTitle = sTitle;
	}

	public int getsDuration() {
		return Integer.parseInt(sDuration.trim());
	}
	
	public int getsDurationInMillis(){
		return Integer.parseInt(sDuration.trim()) * 60 * 1000;
	}

	public void setsDuration(String sDuration) {
		this.sDuration = sDuration;
	}

	public String getsLocation() {
		return sLocation;
	}

	public void setsLocation(String sLocation) {
		this.sLocation = sLocation;
	}

	public String getsFreq() {
		return sFreq;
	}
	
	public int getSessionFrequency(){
		if (sFreq.trim().equalsIgnoreCase("daily"))
			return SESSION_DAILY;
		else if (sFreq.trim().equalsIgnoreCase("weekly"))
			return SESSION_WEEKLY;
		else if (sFreq.trim().equalsIgnoreCase("monthly"))
			return SESSION_MONTHLY;
		return -1;
	}
	
	private static String sessionFrequencyToString(int i){
		if (i == SESSION_DAILY)
			return "daily";
		else if (i == SESSION_WEEKLY)
			return "weekly";
		else if (i == SESSION_MONTHLY)
			return "monthly";
		return "Something's wrong, willis, nice exception handling";
	}

	public void setsFreq(String sFreq) {
		this.sFreq = sFreq;
	}
	
	public SessionTimer getTimer(){
		return sTimer;
	}
	
	@Override
	public String toString() {
		return "Session [sTitle=" + sTitle
				+ ", sDuration=" + sDuration + ", sLocation=" + sLocation
				+ ", sFreq=" + sFreq + "]";
	}

	public int getSessionID() {
		return sessionID;
	}

	public void setSessionID(int sessiondID) {
		this.sessionID = sessiondID;
	}

}
