package uk.gla.sessions.data;

import java.io.Serializable;


public class SessionTimer extends CountDownTimerWithPause implements Serializable{
	
	private static final long serialVersionUID = 3918826508708262422L;
	long millisOnTimer;
	long countDownInterval;
	boolean runAtStart;
	
	//private static final String TAG = "STIMER";
	
	public SessionTimer(long millisOnTimer, long countDownInterval, boolean runAtStart) {
		super(millisOnTimer, countDownInterval, runAtStart);
		this.millisOnTimer = millisOnTimer;
		this.countDownInterval = countDownInterval;
		this.runAtStart = runAtStart;
	}
	
	public SessionTimer(SessionTimer t){
		super(t.millisOnTimer, t.countDownInterval, t.runAtStart);
	}
	
	@Override
	public void onTick(long millisUntilFinished) {
		millisOnTimer = millisUntilFinished;
	}
	
	@Override
	public void onFinish() {
		millisOnTimer = 0;
	}

	public long getMillisOnTimer() {
		return millisOnTimer;
	}
	
	@Override 
	public void onPause(){}
	
	@Override
	public void onResume(){}

	public void setMillisOnTimer(long millisOnTimer) {
		this.millisOnTimer = millisOnTimer;
	}

	public long getCountDownInterval() {
		return countDownInterval;
	}

	public void setCountDownInterval(long countDownInterval) {
		this.countDownInterval = countDownInterval;
	}

	public boolean getRunAtStart() {
		return runAtStart;
	}

	public void setRunAtStart(boolean runAtStart) {
		this.runAtStart = runAtStart;
	}
}
