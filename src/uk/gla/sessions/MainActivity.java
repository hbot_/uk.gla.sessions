package uk.gla.sessions;

import uk.gla.sessions.chart.IDemoChart;
import uk.gla.sessions.chart.TemperatureChart;
import uk.gla.sessions.data.Session;
import uk.gla.sessions.database.SessionsDatabaseHandler;
import uk.gla.sessions.services.TimerService;
import uk.gla.sessions.services.TimerService.TimerBinder;
import android.app.ActionBar;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

public class MainActivity extends FragmentActivity {
	
	private static final String TAG = "MAIN";
	private TimerService tService;
	private boolean tBound = false;
	private Intent tServiceIntent;
	
	SessionFragmentAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;
    ImageButton addButton;
    
    SessionsDatabaseHandler sdb;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tServiceIntent = new Intent(this, TimerService.class);
        startService(tServiceIntent);
        bindService(tServiceIntent, tConnection, Context.BIND_AUTO_CREATE);
        
        setContentView(R.layout.activity_main);
        customizeActionBar();
        addButton = (ImageButton) findViewById(R.id.incrementButton);
        addButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, NewSessionActivity.class);
				startActivityForResult(i, 0);
			}
		});
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (requestCode == 0) {
    		switch (resultCode){
    		case RESULT_OK:
    			Session newSession = new Session(data.getStringArrayExtra("newSession"));
    			writeSessionToDB(newSession);
    			tService.refresh();
    			mAdapter.notifyDataSetChanged();
    			Log.d(TAG, tService.getSessionCount() + "");
    			break;
    		case RESULT_CANCELED:
    			break;
    		default: 
    			break;
    		}
    	}
    }
    
    private static final int MENU_ITEM_VIEW_ALL = 10;
    private static final int MENU_ITEM_STATISTICS = 11;
    private static final int MENU_ITEM_REMOVE = 12;
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		menu.add(0, MENU_ITEM_VIEW_ALL, 0, "View All");
		menu.add(0, MENU_ITEM_STATISTICS, 1, "Statistics");
		menu.add(0, MENU_ITEM_REMOVE, 2, "Remove Session");
		return true;
	}
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item){
    	super.onOptionsItemSelected(item);
		switch(item.getItemId()){
		case MENU_ITEM_VIEW_ALL: break;
		case MENU_ITEM_STATISTICS: showStatistics(); break;
		case MENU_ITEM_REMOVE: break;
		}
		return true;
	}
    
    @Override
    protected void onStart() {
        super.onStart();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        if (tBound) {
            unbindService(tConnection);
            tBound = false;
        }
        tService.stopSelf();
    }
    
    private ServiceConnection tConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            TimerBinder binder = (TimerBinder) service;
            tService = binder.getService();
            tBound = true;
            
            mPager = (ViewPager)findViewById(R.id.pager);
            mAdapter = new SessionFragmentAdapter(getSupportFragmentManager(), tService);
            mPager.setAdapter(mAdapter);
            mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
            mIndicator.setViewPager(mPager);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            tBound = false;
        }
    };
    
    private void customizeActionBar(){
    	 ActionBar aBar = getActionBar();
         aBar.setDisplayShowCustomEnabled(true);
         aBar.setDisplayShowTitleEnabled(false);
         
         LayoutInflater inflator = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         View v = inflator.inflate(R.layout.view_actionbar_title, null);
         TextView bText = (TextView) v.findViewById(R.id.actionbar_title);
         bText.setText("Sessions");
         bText.setTypeface(Typeface.createFromAsset(this.getAssets(), "Dosis-Light.ttf"));
         aBar.setCustomView(v);
    }
    
    private void writeSessionToDB(Session session){
    	SessionsDatabaseHandler sdb = new SessionsDatabaseHandler(this);
    	sdb.addSession(session);
    }
    
    private void showStatistics(){
    	IDemoChart chart = new TemperatureChart();
    	Intent i = chart.execute(this);
    	startActivity(i);
    }
}